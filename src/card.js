import React from 'react';
import './card.css';

class Card extends React.Component {
  render() {
    return (
      <div className="Card">
        <div>
          <img className="user-avatar" src={this.props.user.avatar_url} alt="avatar" />
        </div>
        <div>Name:  
          <a className="user-name" href={this.props.user.web_url}>{this.props.user.name}</a>
        </div>
        <div>Email:  
          <a className="user-username" href={this.props.user.email}>{this.props.user.email}</a>
        </div>
      </div>
    );
  }
}

export default Card;
