
/**
 * Dependencies
 * @ignore
 */
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import './App.css';
import Card from './card';
/**
 * App
 * @ignore
 */
class App extends Component {
  accessToken;
  state = { loading: true, user: null}
  
  componentDidMount() {
    this.asyncLoad().then(response => response.json()).then(json => {
      this.setState({ loading: false, user: json });
    })
  }

  async asyncLoad() {
    window.location.hash = '';
    const { location } = this.props;

    if(!location.hash) {
      const params = new URLSearchParams({
        redirect_uri: window.location.href.slice(0, -1),
        client_id: '366b139c94e01dff2ee54bbdd4d95ab4b592d967250b347cbdaa0e1b57cb040c',
        response_type: 'token'
      });
  
      window.location = `https://gitlab.com/oauth/authorize?${params.toString()}`;
    }

    const token = new URLSearchParams(location.hash.substring(1)).get('access_token');
    if(token) {
      return fetch(
        'https://gitlab.com/api/v4/user',
        {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }
      );
    }
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <main>
          {this.renderMain()}
        </main>
        </header>
      </div>
    );
  }

  renderMain() {
    if(this.state.loading || !this.state.user) {
      return <h1 className="loading">Loading</h1>;
    } else {
      return <Card user={this.state.user} />;
    }
  }

}

export default withRouter(App);

